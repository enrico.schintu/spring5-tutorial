package it.enrico.poc.bootstrap;

import it.enrico.poc.model.jpa.Author;
import it.enrico.poc.model.jpa.Book;
import it.enrico.poc.repositories.jpa.AuthorRepository;
import it.enrico.poc.repositories.jpa.BookRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * DevBootstrap description...
 *
 * @author Enrico Schintu
 * @version 27/10/2019
 */
@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        initData();
    }

    private void initData() {

        // ENRICO
        Author enrico = new Author("Enrico", "Schintu");
        Book book1 = new Book("Il libro di Enrico", "1234", "In Dev Shoes");
        enrico.getBooks().add(book1);
        book1.getAuthors().add(enrico);

        authorRepository.save(enrico);
        bookRepository.save(book1);

        // MARA
        Author mara = new Author("Mara", "Melchiori");
        Book book2 = new Book("Il libro di Mara", "4321", "In Chef Shoes");
        mara.getBooks().add(book2);

        authorRepository.save(mara);
        bookRepository.save(book2);

    }
}
