package it.enrico.poc.repositories.jpa;

import it.enrico.poc.model.jpa.Author;
import org.springframework.data.repository.CrudRepository;

/**
 * AuthorRepository description...
 *
 * @author Enrico Schintu
 * @version 27/10/2019
 */

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
