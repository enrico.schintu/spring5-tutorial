package it.enrico.poc.repositories.jpa;

import it.enrico.poc.model.jpa.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {
}
